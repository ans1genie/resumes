Unnamed microframework to simplify CV(Resume) management

## Features
- Separation of presentation from the data (CV information).
- Support for multiple CVs, one per profile/job offer.
- support for multiple templates.


## How to use
This project is written in Python and depends on `PyYAML` and `jinja2`. You'll also need an XeLaTeX compatible TEX engine ([Tectonic](https://tectonic-typesetting.github.io/en-US/) is recommended).
```
pip install PyYAML jinja2
```

After installing the dependencies, you have to add you information in a subfolder in `overlays`, then specify the folder name in `config.py`

The next step is to patch the tex templates with the data in the yaml files in your overlay
```shell
python patch.py
```

Finally you compile the resulting tex document using your favorite tex engine

```shell
docker run -it -v $PWD:/resume rekka/tectonic tectonic resume.tex
```

> You can delegate all the heavy lifting to gitlab CI/CD, by forking this repository. Each push will trigger a CI/CD pipeline that will patch and build the tex files for you.
